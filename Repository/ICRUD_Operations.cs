﻿// <copyright file="ICRUD_Operations.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

    /// <summary>
    /// ICRUD interface.
    /// </summary>
    public interface ICRUD_Operations
    {
        /// <summary>
        /// Anime studio.
        /// </summary>
        /// <param name="anime_studio">object parameter.</param>
        void Add_anime_studio(ANIME_STUDIOS anime_studio);

        /// <summary>
        /// Character add.
        /// </summary>
        /// <param name="character">object.</param>
        void Add_character(CHARACTERS character);

        /// <summary>
        /// Add anime.
        /// </summary>
        /// <param name="anime">object.</param>
        void Add_anime(ANIMES anime);

        /// <summary>
        /// Remove anime studio.
        /// </summary>
        /// <param name="anime_studio">object.</param>
        void Remove_anime_studio(ANIME_STUDIOS anime_studio);

        /// <summary>
        /// Remove character.
        /// </summary>
        /// <param name="character">object.</param>
        void Remove_character(CHARACTERS character);

        /// <summary>
        /// Remove anime.
        /// </summary>
        /// <param name="anime">object.</param>
        void Remove_anime(ANIMES anime);

        /// <summary>
        /// Update anime studio.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        void Update_anime_studio(int id, string modify);

        /// <summary>
        /// Update extra.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        void Update_character(int id, string modify);

        /// <summary>
        /// Update anime.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        void Update_anime(int id, string modify);

        /// <summary>
        /// Anime to a List.
        /// </summary>
        /// <returns>Anime List.</returns>
        List<ANIME_STUDIOS> GetAllanime_studios();

        /// <summary>
        /// Anime to a List.
        /// </summary>
        /// <returns>Anime List.</returns>
        List<ANIMES> GetAllanimes();

        /// <summary>
        /// Extras to a list.
        /// </summary>
        /// <returns>Character List.</returns>
        List<CHARACTERS> GetAllcharacter();

        /// <summary>
        /// Character by id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Character instance.</returns>
        CHARACTERS GetCharacterByID(int id);

        /// <summary>
        /// Anime by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>Anime instance.</returns>
        ANIME_STUDIOS GetAnimeStudioById(int id);

        /// <summary>
        /// Anime by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>anime instance.</returns>
        ANIMES GetAnimeById(int id);
    }

    /// <summary>
    /// Repo class.
    /// </summary>
    public class Repo : ICRUD_Operations
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repo"/> class.
        /// Repo constructor.
        /// </summary>
        public Repo()
        {
            this.Anime_Studios_Entity = new ANIME_STUDIOS_ENTITY();
        }

        private Database Database { get; set; }

        private ANIME_STUDIOS_ENTITY Anime_Studios_Entity { get; set; }

        /// <summary>
        /// Add anime studio.
        /// </summary>
        /// <param name="anime_studio">object manufacturer.</param>
        public void Add_anime_studio(ANIME_STUDIOS anime_studio)
        {
            this.Anime_Studios_Entity.ANIME_STUDIOS.Add(anime_studio);
            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Add character.
        /// </summary>
        /// <param name="character">object extra.</param>
        public void Add_character(CHARACTERS character)
        {
            this.Anime_Studios_Entity.CHARACTERS.Add(character);
            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Add anime.
        /// </summary>
        /// <param name="anime">object model.</param>
        public void Add_anime(ANIMES anime)
        {
            this.Anime_Studios_Entity.ANIMES.Add(anime);
            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Remove anime studio.
        /// </summary>
        /// <param name="anime_studio">object.</param>
        public void Remove_anime_studio(ANIME_STUDIOS anime_studio)
        {
            ANIME_STUDIOS example = this.Anime_Studios_Entity.ANIME_STUDIOS.Where(x => x.ID == anime_studio.ID).Single();
            this.Anime_Studios_Entity.ANIME_STUDIOS.Remove(example);
            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Remove character.
        /// </summary>
        /// <param name="character">object extra.</param>
        public void Remove_character(CHARACTERS character)
        {
            CHARACTERS example = this.Anime_Studios_Entity.CHARACTERS.Where(x => x.ID == character.ID).Single();

            this.Anime_Studios_Entity.CHARACTERS.Remove(example);
            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Remove anime.
        /// </summary>
        /// <param name="animes">object model.</param>
        public void Remove_anime(ANIMES animes)
        {
            ANIMES example = this.Anime_Studios_Entity.ANIMES.Where(x => x.ID == animes.ID).Single();
            this.Anime_Studios_Entity.ANIMES.Remove(example);
            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Update anime studio.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        public void Update_anime_studio(int id, string modify)
        {
            string[] ertekek = modify.Split(',');

            IQueryable<ANIME_STUDIOS> a = from x in this.Anime_Studios_Entity.ANIME_STUDIOS
                    where x.ID == id
                    select x;

            foreach (var item in a)
            {
                if (ertekek[0] != "null")
                {
                    item.ID = int.Parse(ertekek[0]);
                }

                if (ertekek[1] != "null")
                {
                    item.NEV = ertekek[1];
                }

                if (ertekek[2] != "null")
                {
                    item.KOZPONT = ertekek[2];
                }

                if (ertekek[3] != "null")
                {
                    item.ROMANIZALT_NEV = ertekek[3];
                }

                if (ertekek[4] != "null")
                {
                    item.ALAPITAS_EVE = int.Parse(ertekek[4]);
                }

                if (ertekek[5] != "null")
                {
                    item.TULAJDONOS = ertekek[5];
                }

                if (ertekek[6] != "null")
                {
                    item.DOLGOZOK_SZAMA = int.Parse(ertekek[6]);
                }
            }

            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Update character.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        public void Update_character(int id, string modify)
        {
            string[] ertekek = modify.Split(',');

            // "id    kategórianév    név ár  szín    többször használható e"
            IQueryable<CHARACTERS> a = from x in this.Anime_Studios_Entity.CHARACTERS
                    where x.ID == id
                    select x;

            foreach (var item in a)
            {
                if (ertekek[0] != "null")
                {
                    item.ID = int.Parse(ertekek[0]);
                }

                if (ertekek[1] != "null")
                {
                    item.ANIME_ID = int.Parse(ertekek[1]);
                }

                if (ertekek[2] != "null")
                {
                    item.NEV = ertekek[2];
                }

                if (ertekek[3] != "null")
                {
                    item.ROMANIZALT_NEV = ertekek[3];
                }

                if (ertekek[4] != "null")
                {
                    item.LEGFOBB_TULAJDONSAG = ertekek[4];
                }

                if (ertekek[5] != "null")
                {
                    item.KOZVETITES_EVE = int.Parse(ertekek[5]);
                }

                if (ertekek[6] != "null")
                {
                    item.RUHAZAT_JELLEGE = ertekek[6];
                }
            }

            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Update anime.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        public void Update_anime(int id, string modify)
        {
            string[] ertekek = modify.Split(',');

            // "marka_id=0   modell_id=1 megjelenes napja=2  motorterfogat=3 loero=4 alapar=5"
            IQueryable<ANIMES> a = from x in this.Anime_Studios_Entity.ANIMES
                    where x.ID == id
                    select x;

            foreach (var item in a)
            {
                if (ertekek[0] != "null")
                {
                    item.ID = int.Parse(ertekek[0]);
                }

                if (ertekek[1] != "null")
                {
                    item.STUDIO_ID = int.Parse(ertekek[1]);
                }

                if (ertekek[2] != "null")
                {
                    item.NEV = ertekek[2];
                }

                if (ertekek[3] != "null")
                {
                    item.ROMANIZALT_NEV = ertekek[3];
                }

                if (ertekek[4] != "null")
                {
                    item.FORRAS_ANYAG = ertekek[4];
                }

                if (ertekek[5] != "null")
                {
                    item.EVADOK_SZAMA = int.Parse(ertekek[5]);
                }

                if (ertekek[6] != "null")
                {
                    item.KOZVETITES_EVE = int.Parse(ertekek[6]);
                }
            }

            this.Anime_Studios_Entity.SaveChanges();
        }

        /// <summary>
        /// Get all anime studio.
        /// </summary>
        /// <returns>anime studio list.</returns>
        public List<ANIME_STUDIOS> GetAllanime_studios()
        {
            return this.Anime_Studios_Entity.ANIME_STUDIOS.ToList();
        }

        /// <summary>
        /// Get all anime.
        /// </summary>
        /// <returns>anime list.</returns>
        public List<ANIMES> GetAllanimes()
        {
            return this.Anime_Studios_Entity.ANIMES.ToList();
        }

        /// <summary>
        /// Get all character.
        /// </summary>
        /// <returns>character list.</returns>
        public List<CHARACTERS> GetAllcharacter()
        {
            return this.Anime_Studios_Entity.CHARACTERS.ToList();
        }

        /// <summary>
        /// Get charaacter id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Character instance.</returns>
        public CHARACTERS GetCharacterByID(int id)
        {
            return this.GetAllcharacter().Where(x => x.ID == id).Single();
        }

        /// <summary>
        /// Anime by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>Anime instance.</returns>
        public ANIME_STUDIOS GetAnimeStudioById(int id)
        {
            return this.GetAllanime_studios().Where(x => x.ID == id).Single();
        }

        /// <summary>
        /// Anime by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>anime instance.</returns>
        public ANIMES GetAnimeById(int id)
        {
            return this.GetAllanimes().Where(x => x.ID == id).Single();
        }
    }
}
