﻿INSERT INTO ANIME_STUDIOS(ID,NEV,ROMANIZALT_NEV,ALAPITAS_EVE,KOZPONT,TULAJDONOS,DOLGOZOK_SZAMA)
 VALUES (1,'Kyoto Animation','Ki-o-to A-ni-ma-ti-on-o',1990,'Tokyo','Aruka Hideki',10);

INSERT INTO ANIME_STUDIOS(ID,NEV,ROMANIZALT_NEV,ALAPITAS_EVE,KOZPONT,TULAJDONOS,DOLGOZOK_SZAMA)
 VALUES (2,'Madhouse','Ma-do-ho-u-so',1995,'Tokyo','Tomoya Akiro',100);

 INSERT INTO ANIME_STUDIOS(ID,NEV,ROMANIZALT_NEV,ALAPITAS_EVE,KOZPONT,TULAJDONOS,DOLGOZOK_SZAMA)
 VALUES (3,'Toei Animation','To-e-i A-ni-ma-ti-o-no',1980,'Kyoto','Toaru Kisoto',56);

 INSERT INTO ANIME_STUDIOS(ID,NEV,ROMANIZALT_NEV,ALAPITAS_EVE,KOZPONT,TULAJDONOS,DOLGOZOK_SZAMA)
 VALUES (4,'A1 Studio','A1 Su-to-di-o',2000,'Suzuka','Kamijo Touma',120);

 INSERT INTO ANIME_STUDIOS(ID,NEV,ROMANIZALT_NEV,ALAPITAS_EVE,KOZPONT,TULAJDONOS,DOLGOZOK_SZAMA)
 VALUES (5,'Ufotable','U-fo-ta-bu-ru',2010,'Fuji','Silica Fuzaku',45);



 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(1,1,'Melancholy of Haruhi Suzumiya','Su-zu-mi-ya no Yu-u-tsu','Light Novel',4,2006)

 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(2,1,'Lucky Star','Ra-kki Su-ta-ru','Manga',2,2006)

INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(3,1,'K-ON','K-On','Manga',4,2013)

 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(4,1,'Clannad','Ku-ra-no-na-do','Manga',3,2010)



 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(5,2,'One Punch Man','O-ne Pu-no-tso Ma-no','Manga',4,2016)

 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(6,2,'Devil Man Crybaby','De-vi-ru Ma-no Ku-ra-yu-ba-by','Manga',1,2017)

 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(7,2,'Kill la Kill','Ki-ru ra Ki-ru','Light Novel',4,2012)

 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(8,2,'Flip Flappers','Fu-ri-ppu Fu-rappa','Light Novel',1,2010)

 INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(9,3,'One Piece','O-ne Pu-i-szu','Manga',81,1999)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(10,3,'D Gray Man','Do Gu-ra-yo Ma-nu','Game',10,2005)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(11,3,'Detective Conan','De-te-ku-ti-vu Ko-na-nu','Manga',40,1981)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(12,3,'Dragon Ball','Do-ra-go-nu Bo-ru','Manga',17,1995)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(13,4,'Akame Ga Kill','A-ka-me Ga Ku-ri-ru','Manga',2,2011)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(14,4,'Sword Art Online','So-do-a-to On-ra-i-nu','Light Novel',8,2012)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(15,4,'Monogatari Se-ri-e-su','Mo-no-ga-ta-ri se-ri-es','Light Novel',20,2009)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(16,4,'No Game No Life','No ga-mu No Ra-i-fu','Manga',1,2014)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(17,5,'Demon Slayer','Ki-me-tsu no Ya-i-ba','Manga',2,2019)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(18,5,'Hunter X Hunter','Hu-no-ta e-ku-szu Hu-no-ta','Manga',11,2010)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(19,5,'Konosuba','Ko-no-su-ba','Light Novel',2,2016)

  INSERT INTO ANIMES(ID,STUDIO_ID,NEV,ROMANIZALT_NEV,FORRAS_ANYAG,EVADOK_SZAMA,KOZVETITES_EVE)
 VALUES(20,5,'My sister cant be this cute','Ore no i-mo-u-to wa ko-nna-ni ka-wa-i wa-ke-ja na-i','Light Novel',2,2014)

 INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(1,1,'Haruhi Suzumiya','Ha-ru-hi Su-zu-mi-ya','barna','hiperaktiv',2006,'iskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(2,1,'Yuuki Nagato','Yu-u-ki Na-ga-to ','szürke','csendes',2006,'iskolai')

 INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(3,2,'Konata Izumi','Ko-na-ta I-zu-mi','kék','otaku',2006,'iskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(4,2,'Kuroi sensei','Ku-ro-i se-n-se-i','barna','szigorú, gamer',2006,'tanári')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(5,3,'Hirasawa Yui','Hi-ra-sa-wa Yu-i','barna','aranyos,',2013,'keleti-középiskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(6,3,'Asakaya Mio','A-sa-ka-ya Mi-o','fekete','komoly,félénk',2013,'keleti-középiskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(7,4,'Nagisa chan','Na-gi-sa cha-n','barna','csendes,félénk',2010,'iskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(8,4,'Tomoya kun','To-mo-ya kun','barna','komoly,tisztelettudó',2010,'középiskolai')

 INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(9,5,'Saitama sensei','Sa-i-ta-ma sen-se-i','kopasz','nyugodt, magas vérmérsékletű',2016,'iskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(10,5,'Genos','Ge-no-su','szőke','hiperaktiv,lojális',2016,'robot')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(11,6,'Kogaro Atsuya','Ko-ga-ro A-tsu-ya','barna','megontolt, sportimádó',2017,'rövidujjú')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(12,6,'Kemono Chira','Ke-mo-no Chi-ra','vörös','aljas,gazdag',2017,'szőrme')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(13,7,'Ryuko Matoi','Ryu-ko Ma-to-i','fekete','hiperaktiv,tehetséges',2012,'fekete-piros,iskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(14,7,'Mako Natsuya','Ma-ko Na-tsu-ya','világosbarna','hiperaktiv,okos',2012,'iskolai,kék-fehér')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(15,8,'Kokona','Ko-ko-na','kék','félénk,megfontolt',2010,'alsó-középiskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(16,8,'Papika','Pa-pi-ka','narancs','hiperaktiv,kedves',2010,'fehér-nyakkendős')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(17,9,'Luffy','Luffy','fekete','hiperaktiv,careless',1999,'kalóz')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(18,9,'Nami','Na-mi','narancs','szép,térképkészítő',1999,'női rövidujjú')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(19,10,'Konsetsu Koya','Ko-n-se-tsu Ko-ya','kék','cseles,magas',2005,'katonai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(20,10,'Kagiru Amari','Ka-gi-ru A-ma-ri','lila','tehetséges, szegény',2005,'szakadt, rongy')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(21,11,'Conan','Ko-na-no','fekete','detektív, nagy szemüveg',1981,'öltöny')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(22,11,'Kampiru','Ka-mi-pi-ru','kék-fehér','ideges,felfedező',1981,'bőr, barna')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(23,12,'Goku','Go-ku','fekete','hiperaktiv,meggondolatlan',1995,'narancs-fekete, szakadt')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(24,12,'Frieza','Fu-ri-e-za','kopasz','egoista',1995,'nincs')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(25,13,'Akame','A-ka-me','fekete','csendes gyilkos, hosszú haj',2011,'fekete-piros')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(26,13,'Tatsuma','Ta-tsu-ma','világosbarna','kalandor, jókedvű, magas igazságérzet',2011,'sárga-fehér övvel')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(27,14,'Kirigaya Kazuto','Ki-ri-ga-ya Ka-zu-to','fekete','gamer, overpowered',2012,'iskolai')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(28,14,'Yuuki Asuna','Yu-u-ki A-su-na','szőke','szép,okos,céltudatos',2012,'keresztes lovagi ruha')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(29,15,'Araragi Koyomi','A-ra-ra-gi Ko-yo-mi','fekete','céltalan, perverz,igazságos',2009,'utcai normál')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(30,15,'Senjougahara Hitagi','Sen-yo-u-ha-ga-ra Hi-ta-gi','lila','komoly, törődő',2009,'rövidujjú,szoknya')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(31,16,'Sora','So-ra','piros','okos, ravasz, ',2014,'I love humanity póló')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(32,16,'Shiro','Shi-ro','világoskék','törpe,aranyos, húg',2014,'szoknya,egyrészes')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(33,17,'Tanjiro','Ta-nu-ji-ro','fekete-piros','nyugodt, céltudatos',2019,'harci ruházat')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(34,17,'Nezuko','Ne-zu-ko','fekete','dobozba zárt,démon',2019,'rózsaszin,egyrészes')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(35,18,'Gon','Go-nu','zöld','erős, fürge',2010,'zöld egyrészes')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(36,18,'Killua','Ki-ru-a','szürke','komoly,erős',2010,'fehér fölső,kék nadrág')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(37,19,'Kazuma','Ka-zu-ma','barna','nemek egyenjoguságában hisz,perverz',2016,'kalandor')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(38,19,'Aqua','A-ku-a','kék','Axis egyház istennője',2016,'egyrészes,kék rövid szoknya')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(39,20,'Kousaka Kirino','Ko-u-sa-ka Ki-ri-no','narancs','húg,modell,jó sportoló',2014,'iskolai egyenruha')

  INSERT INTO CHARACTERS(ID,ANIME_ID,NEV,ROMANIZALT_NEV,HAJSZIN,LEGFOBB_TULAJDONSAG,KOZVETITES_EVE,RUHAZAT_JELLEGE)
 VALUES(40,20,'Kyosuke Kosaka','Ko-yo-su-ke Ko-sa-ka','barna','megontolt,báty',2014,'fehér ing, barna nadrág')







 


 

 




 








 



