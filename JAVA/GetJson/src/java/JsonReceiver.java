/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Beni
 */
public class JsonReceiver extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String paramName = "db";
        String paramValue = request.getParameter(paramName);
 
       if("6".equals(paramValue))
       {
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("{\n" +
"\"contents\":\n" +
"	[\n" +
"	{\n" +
"	  \"ID\": 20,\n" +
"	  \"STUDIO_ID\": 15,\n" +
"	  \"NEV\": \"Shinsekai Yori\",\n" +
"	  \"ROMANZALT_NEV\": \"Shi-no-se-ka-i Yo-ri\",\n" +
"	  \"FORRAS_ANYAG\": \"Light Novel\",\n" +
"	  \"EVADOK_SZAMA\": 4,\n" +
"	  \"KOZVETITES_EVE\": 2012\n" +
"	},\n" +
"\n" +
"	{\n" +
"	  \"ID\": 21,\n" +
"	  \"STUDIO_ID\": 16,\n" +
"	  \"NEV\": \"Hitoribocchi no Seikatsu\",\n" +
"	  \"ROMANZALT_NEV\": \"Hi-to-ri Bo-cchi no Se-i-ka-tsu\",\n" +
"	  \"FORRAS_ANYAG\": \"Manga\",\n" +
"	  \"EVADOK_SZAMA\": 1,\n" +
"	  \"KOZVETITES_EVE\": 2019\n" +
"	},\n" +
"{\n" +
"	  \"ID\": 22,\n" +
"	  \"STUDIO_ID\": 17,\n" +
"	  \"NEV\": \"Inuyasha\",\n" +
"	  \"ROMANZALT_NEV\": \"I-nu-ya-sha\",\n" +
"	  \"FORRAS_ANYAG\": \"Manga\",\n" +
"	  \"EVADOK_SZAMA\": 20,\n" +
"	  \"KOZVETITES_EVE\": 2000\n" +
"	},\n" +
"{\n" +
"	  \"ID\": 23,\n" +
"	  \"STUDIO_ID\": 18,\n" +
"	  \"NEV\": \"Citrus\",\n" +
"	  \"ROMANZALT_NEV\": \"Ci-to-ru-so\",\n" +
"	  \"FORRAS_ANYAG\": \"Manga\",\n" +
"	  \"EVADOK_SZAMA\": 1,\n" +
"	  \"KOZVETITES_EVE\": 2017\n" +
"	},\n" +
"    {\n" +
"	  \"ID\": 24,\n" +
"	  \"STUDIO_ID\": 19,\n" +
"	  \"NEV\": \"Tatami Galaxy\",\n" +
"	  \"ROMANZALT_NEV\": \"Ta-ta-mi Ga-la-ku-szi\",\n" +
"	  \"FORRAS_ANYAG\": \"Light Novel\",\n" +
"	  \"EVADOK_SZAMA\": 1,\n" +
"	  \"KOZVETITES_EVE\": 2007\n" +
"	},\n" +
"{\n" +
"	  \"ID\": 25,\n" +
"	  \"STUDIO_ID\": 20,\n" +
"	  \"NEV\": \"Ben-to\",\n" +
"	  \"ROMANZALT_NEV\": \"Be-no-to\",\n" +
"	  \"FORRAS_ANYAG\": \"Light Novel\",\n" +
"	  \"EVADOK_SZAMA\": 12,\n" +
"	  \"KOZVETITES_EVE\": 2010\n" +
"}\n" +
"	\n" +
"	]\n" +
"}");
       }
       }
        else if("3".equals(paramValue))
        {
           try (PrintWriter out = response.getWriter()) {
                out.println("{ \n" +
"   \"contents\":[ \n" +
"      { \n" +
"         \"ID\":20,\n" +
"         \"STUDIO_ID\":15,\n" +
"         \"NEV\":\"Shinsekai Yori\",\n" +
"         \"ROMANZALT_NEV\":\"Shi-no-se-ka-i Yo-ri\",\n" +
"         \"FORRAS_ANYAG\":\"Light Novel\",\n" +
"         \"EVADOK_SZAMA\":4,\n" +
"         \"KOZVETITES_EVE\":2012\n" +
"      },\n" +
"      { \n" +
"         \"ID\":21,\n" +
"         \"STUDIO_ID\":16,\n" +
"         \"NEV\":\"Hitoribocchi no Seikatsu\",\n" +
"         \"ROMANZALT_NEV\":\"Hi-to-ri Bo-cchi no Se-i-ka-tsu\",\n" +
"         \"FORRAS_ANYAG\":\"Manga\",\n" +
"         \"EVADOK_SZAMA\":1,\n" +
"         \"KOZVETITES_EVE\":2019\n" +
"      },\n" +
"      { \n" +
"         \"ID\":22,\n" +
"         \"STUDIO_ID\":17,\n" +
"         \"NEV\":\"Inuyasha\",\n" +
"         \"ROMANZALT_NEV\":\"I-nu-ya-sha\",\n" +
"         \"FORRAS_ANYAG\":\"Manga\",\n" +
"         \"EVADOK_SZAMA\":20,\n" +
"         \"KOZVETITES_EVE\":2000\n" +
"      },\n" +
"   ]\n" +
"}\n" +
"");
                }       
        
       
       
        }
         
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
