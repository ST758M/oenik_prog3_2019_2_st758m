var indexSectionsWithContent =
{
  0: "acdefgijklmnoprstuv",
  1: "acdijlrt",
  2: "lort",
  3: "acdgilmprstu",
  4: "acefiknrsv",
  5: "cnr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Pages"
};

