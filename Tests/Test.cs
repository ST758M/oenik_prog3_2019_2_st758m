﻿// <copyright file="Test.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class Test
    {
        private Logic l;
        private List<ANIME_STUDIOS> animeStudioList;
        private List<CHARACTERS> characterList;
        private List<ANIMES> animeList;
        private Mock<Repository.ICRUD_Operations> mockedRepo;

        /// <summary>
        /// Init.
        /// </summary>
        [SetUp]
        public void SetupLogic()
        {
            this.mockedRepo =
              new Mock<Repository.ICRUD_Operations>();

            this.l = new Logic(this.mockedRepo.Object);
            this.animeStudioList = new List<ANIME_STUDIOS>();
            this.characterList = new List<CHARACTERS>();
            this.animeList = new List<ANIMES>();

            ANIME_STUDIOS instance = new ANIME_STUDIOS()
            {
                ID = 1,
                NEV = "Kyoto Animation",
                ALAPITAS_EVE = 1981,
                DOLGOZOK_SZAMA = 200,
                KOZPONT = "Kyoto",
                ROMANIZALT_NEV = "Ki-yo-to A-ni-me-sho-n",
                TULAJDONOS = "Hidesaki Hatta",
            };
            this.animeStudioList.Add(instance);
            instance = new ANIME_STUDIOS()
            {
                ID = 2,
                NEV = "Madhouse",
                ALAPITAS_EVE = 1986,
                DOLGOZOK_SZAMA = 180,
                KOZPONT = "Kyoto",
                ROMANIZALT_NEV = "Ma-do-ho-u-se",
                TULAJDONOS = "Kibuki Koto",
            };
            this.animeStudioList.Add(instance);
            instance = new ANIME_STUDIOS()
            {
                ID = 3,
                NEV = "A1 Pictures",
                ALAPITAS_EVE = 1975,
                DOLGOZOK_SZAMA = 230,
                KOZPONT = "Tokio",
                ROMANIZALT_NEV = "A1 Pi-tsu-tu-ro-so",
                TULAJDONOS = "Yabuki Otonashi",
            };
            this.animeStudioList.Add(instance);
            instance = new ANIME_STUDIOS()
            {
                ID = 4,
                NEV = "Ufotable",
                ALAPITAS_EVE = 2005,
                DOLGOZOK_SZAMA = 130,
                KOZPONT = "Sapporo",
                ROMANIZALT_NEV = "U-fo-ta-bo-ro",
                TULAJDONOS = "Kirigaya Tokei",
            };
            this.animeStudioList.Add(instance);
            instance = new ANIME_STUDIOS()
            {
                ID = 5,
                NEV = "Studio Bones",
                ALAPITAS_EVE = 1986,
                DOLGOZOK_SZAMA = 180,
                KOZPONT = "Sakura",
                ROMANIZALT_NEV = "Su-to-di-o Bo-ne-su",
                TULAJDONOS = "Araragi Yakyu",
            };
            this.animeStudioList.Add(instance);
            CHARACTERS instance_1 = new CHARACTERS()
            {
                ID = 1,
                ANIME_ID = 3,
                HAJSZIN = "Fekete",
                KOZVETITES_EVE = 2012,
                LEGFOBB_TULAJDONSAG = "overpowered",
                NEV = "Kirigaya Kazuto",
                ROMANIZALT_NEV = "Ki-ri-ga-ya Ka-zu-to",
                RUHAZAT_JELLEGE = "Fekete, lovagi",
            };
            this.characterList.Add(instance_1);
            instance_1 = new CHARACTERS()
            {
                ID = 2,
                ANIME_ID = 2,
                HAJSZIN = "Kopasz",
                KOZVETITES_EVE = 2016,
                LEGFOBB_TULAJDONSAG = "careless",
                NEV = "Saitama",
                ROMANIZALT_NEV = "Sa-i-ta-ma",
                RUHAZAT_JELLEGE = "szuperhős, köpeny",
            };
            this.characterList.Add(instance_1);
            instance_1 = new CHARACTERS()
            {
                ID = 2,
                ANIME_ID = 5,
                HAJSZIN = "fekete",
                KOZVETITES_EVE = 2006,
                LEGFOBB_TULAJDONSAG = "túlpörgő",
                NEV = "Suzumiya Haruhi",
                ROMANIZALT_NEV = "Su-zu-mi-ya Ha-ru-hi",
                RUHAZAT_JELLEGE = "iskolai egyenruha",
            };
            this.characterList.Add(instance_1);
            instance_1 = new CHARACTERS()
            {
                ID = 2,
                ANIME_ID = 2,
                HAJSZIN = "kék",
                KOZVETITES_EVE = 2006,
                LEGFOBB_TULAJDONSAG = "neet",
                NEV = "Konata Izumi",
                ROMANIZALT_NEV = "Ko-na-ta I-zu-mi",
                RUHAZAT_JELLEGE = "középiskolai egyenruha",
            };
            this.characterList.Add(instance_1);
            instance_1 = new CHARACTERS()
            {
                ID = 2,
                ANIME_ID = 3,
                HAJSZIN = "szőke",
                KOZVETITES_EVE = 2012,
                LEGFOBB_TULAJDONSAG = "erős női karakter",
                NEV = "Yuuki Asuna",
                ROMANIZALT_NEV = "Yu-u-ki A-su-na",
                RUHAZAT_JELLEGE = "fehér-vörös köpeny, lovagi kereszt",
            };
            this.characterList.Add(instance_1);
            ANIMES instance_2 = new ANIMES()
            {
                STUDIO_ID = 3,
                EVADOK_SZAMA = 4,
                FORRAS_ANYAG = "Light Novel",
                ID = 3,
                KOZVETITES_EVE = 2012,
                NEV = "Sword Art Online",
                ROMANIZALT_NEV = "So-do-a-to on-ri-ne-u",
            };
            this.animeList.Add(instance_2);
            instance_2 = new ANIMES()
            {
                STUDIO_ID = 1,
                EVADOK_SZAMA = 4,
                FORRAS_ANYAG = "Manga",
                ID = 2,
                KOZVETITES_EVE = 2006,
                NEV = "Lucky Star",
                ROMANIZALT_NEV = "Ru-kki Su-ta-ru",
            };
            this.animeList.Add(instance_2);
            instance_2 = new ANIMES()
            {
                STUDIO_ID = 2,
                EVADOK_SZAMA = 2,
                FORRAS_ANYAG = "Manga",
                ID = 1,
                KOZVETITES_EVE = 2016,
                NEV = "One Punch Man",
                ROMANIZALT_NEV = "O-ne pu-nu-tso ma-nu",
            };
            this.animeList.Add(instance_2);
            instance_2 = new ANIMES()
            {
                STUDIO_ID = 1,
                EVADOK_SZAMA = 4,
                FORRAS_ANYAG = "Manga",
                ID = 5,
                KOZVETITES_EVE = 2006,
                NEV = "Melancholy of Haruhi Suzumiya",
                ROMANIZALT_NEV = "Ha-ru-hi no-yu-u-tsu",
            };
            this.animeList.Add(instance_2);
            instance_2 = new ANIMES()
            {
                STUDIO_ID = 4,
                EVADOK_SZAMA = 1,
                FORRAS_ANYAG = "Manga",
                ID = 1,
                KOZVETITES_EVE = 2019,
                NEV = "Demon Slayer",
                ROMANIZALT_NEV = "Ki-me-tsu no Ya-i-ba",
            };
            this.animeList.Add(instance_2);

            this.mockedRepo.Setup(repo => repo.GetAllanime_studios())
                .Returns(this.animeStudioList);
            this.mockedRepo.Setup(repo => repo.GetAllanimes())
                .Returns(this.animeList);
            this.mockedRepo.Setup(repo => repo.GetAllcharacter())
                .Returns(this.characterList);
        }

        /// <summary>
        /// A character added to list.
        /// </summary>
        [Test]
        public void AddCharacter()
        {
            CHARACTERS instance = new CHARACTERS()
            {
                ID = 20,
                ANIME_ID = 3,
                HAJSZIN = "zöld",
                KOZVETITES_EVE = 2012,
                LEGFOBB_TULAJDONSAG = "sniper skill",
                NEV = "Asada Shinon",
                ROMANIZALT_NEV = "A-sa-da Shi-no-n",
                RUHAZAT_JELLEGE = "fehér-zöld pántos ruházat",
            };
            this.l.Add_character(instance);
            this.mockedRepo.Verify(x => x.Add_character(instance));
        }

        /// <summary>
        /// Anime added to list.
        /// </summary>
        [Test]
        public void AddAnime()
        {
            ANIMES instance_2 = new ANIMES()
            {
                STUDIO_ID = 4,
                EVADOK_SZAMA = 1,
                FORRAS_ANYAG = "Manga",
                ID = 6,
                KOZVETITES_EVE = 1995,
                NEV = "Sailor Moon",
                ROMANIZALT_NEV = "Sa-i-ro-ru Mo-o-nu",
            };
            this.l.Add_anime(instance_2);
            this.mockedRepo.Verify(x => x.Add_anime(instance_2));
        }

        /// <summary>
        /// Anime added to list.
        /// </summary>
        [Test]
        public void AddAnimeStudio()
        {
            ANIME_STUDIOS instance_2 = new ANIME_STUDIOS()
            {
                ID = 6,
                NEV = "Toei Animation",
                ALAPITAS_EVE = 1980,
                DOLGOZOK_SZAMA = 180,
                KOZPONT = "Tokyo",
                ROMANIZALT_NEV = "Toei Animation",
                TULAJDONOS = "Goran Zabuza",
            };
            this.l.Add_anime_studio(instance_2);
            this.mockedRepo.Verify(x => x.Add_anime_studio(instance_2));
        }

        /// <summary>
        /// Character remove test.
        /// </summary>
        [Test]
        public void RemoveCharacter()
        {
            CHARACTERS instance_1 = new CHARACTERS()
            {
                ID = 2,
                ANIME_ID = 3,
                HAJSZIN = "szőke",
                KOZVETITES_EVE = 2012,
                LEGFOBB_TULAJDONSAG = "erős női karakter",
                NEV = "Yuuki Asuna",
                ROMANIZALT_NEV = "Yu-u-ki A-su-na",
                RUHAZAT_JELLEGE = "fehér-vörös köpeny, lovagi kereszt",
            };

            this.l.Remove_character(instance_1);
            this.mockedRepo.Verify(x => x.Remove_character(instance_1));
        }

        /// <summary>
        /// Anime remove test.
        /// </summary>
        [Test]
        public void RemoveAnime()
        {
            ANIMES instance_2 = new ANIMES()
            {
                STUDIO_ID = 4,
                EVADOK_SZAMA = 1,
                FORRAS_ANYAG = "Manga",
                ID = 1,
                KOZVETITES_EVE = 2019,
                NEV = "Demon Slayer",
                ROMANIZALT_NEV = "Ki-me-tsu no Ya-i-ba",
            };

            this.l.Remove_anime(instance_2);
            this.mockedRepo.Verify(x => x.Remove_anime(instance_2));
        }

        /// <summary>
        /// Anime studio remove test.
        /// </summary>
        [Test]
        public void RemoveAnimeStudio()
        {
            ANIME_STUDIOS instance = new ANIME_STUDIOS()
            {
                ID = 3,
                NEV = "A1 Pictures",
                ALAPITAS_EVE = 1975,
                DOLGOZOK_SZAMA = 230,
                KOZPONT = "Tokio",
                ROMANIZALT_NEV = "A1 Pi-tsu-tu-ro-so",
                TULAJDONOS = "Yabuki Otonashi",
            };

            this.l.Remove_anime_studio(instance);
            this.mockedRepo.Verify(x => x.Remove_anime_studio(instance));
        }

        /// <summary>
        /// Update character test.
        /// </summary>
        [Test]
        public void UpdateCharacter()
        {
            int id = 2;
            string modify = "null,null,null,null,null,null,2013,null";
            this.l.Update_character(id, modify);
            this.mockedRepo.Verify(x => x.Update_character(id, modify));
        }

        /// <summary>
        /// Update anime test.
        /// </summary>
        [Test]
        public void UpdateAnime()
        {
            int id = 6;
            string modify = "null,null,null,null,null,Light novel,null,null";
            this.l.Update_anime(id, modify);
            this.mockedRepo.Verify(x => x.Update_anime(id, modify));
        }

        /// <summary>
        /// Update anime studio test.
        /// </summary>
        [Test]
        public void UpdateAnimeStudio()
        {
            int id = 1;
            string modify = "null,null,null,null,1990,null,null";
            this.l.Update_anime_studio(id, modify);
            this.mockedRepo.Verify(x => x.Update_anime_studio(id, modify));
        }

        /// <summary>
        /// A null value added should throw exception.
        /// </summary>
        [Test]
        public void AddNullValue()
        {
            Assert.That(() => this.l.Add_character(null), Throws.ArgumentNullException);
        }

        /// <summary>
        /// All created year should be in this target.
        /// </summary>
        [Test]
        public void CreatedYearValueRange()
        {
            List<ANIME_STUDIOS> a = this.l.GetAllanime_studios();
            Assert.That(a.Count == a.Where(x => x.ALAPITAS_EVE <= 2019 && x.ALAPITAS_EVE >= 1917).Count());
        }

        /// <summary>
        /// Two instance have the same Id.
        /// </summary>
        [Test]
        public void TwoInstanceSameId()
        {
            CHARACTERS instance_1 = new CHARACTERS()
            {
                ID = 2,
                ANIME_ID = 3,
                HAJSZIN = "szőke",
                KOZVETITES_EVE = 2012,
                LEGFOBB_TULAJDONSAG = "erős női karakter",
                NEV = "Yuuki Asuna",
                ROMANIZALT_NEV = "Yu-u-ki A-su-na",
                RUHAZAT_JELLEGE = "fehér-vörös köpeny, lovagi kereszt",
            };

            Assert.That(() => this.l.Add_character(instance_1), Throws.Exception);
        }

        /// <summary>
        /// Database fill test.
        /// </summary>
        [Test]
        public void MockDatabaseFill()
        {
            int animeCount = this.l.Count_anime();
            int animeStudioCount = this.l.Count_anime_studio();
            int characterCount = this.l.Count_character();
            Assert.That(animeCount == 5 && animeStudioCount == 5 && characterCount == 5);
        }

        /// <summary>
        /// Romanized name cant contain letter test.
        /// </summary>
        [Test]
        public void RomanizedNameCantContainLetter()
        {
            bool isContain = false;
            List<ANIMES> animes = this.l.GetAllanimes();
            List<ANIME_STUDIOS> animeStudios = this.l.GetAllanime_studios();
            List<CHARACTERS> characters = this.l.GetAllcharacter();

            foreach (var item in animes)
            {
                if (item.ROMANIZALT_NEV.Contains("L"))
                {
                    isContain = true;
                }
            }

            foreach (var item in animeStudios)
            {
                if (item.ROMANIZALT_NEV.Contains("L"))
                {
                    isContain = true;
                }
            }

            foreach (var item in characters)
            {
                if (item.ROMANIZALT_NEV.Contains("L"))
                {
                    isContain = true;
                }
            }

            Assert.That(isContain == false);
        }

        /// <summary>
        /// Is anime contain black haired character.
        /// </summary>
        [Test]
        public void IsAnimeContainBlackHairedCharacter()
        {
            List<IsAnimeContainBlackHairedCharacter> instanceList = new List<IsAnimeContainBlackHairedCharacter>();
            instanceList = this.l.IsAnimeContainBlackHairedCharacter();
            int containedAnimeCount = 0;
            foreach (var item in instanceList)
            {
                if (item.Van_e)
                {
                    containedAnimeCount++;
                }
            }

            Assert.That(containedAnimeCount == 1);
        }

        /// <summary>
        /// Test Anime per anime studios number.
        /// </summary>
        [Test]
        public void AnimePerAnimeStudios()
        {
         List<AnimePerAnimeStudios> list = this.l.AnimePerAnimeStudios();
         int expectKyotoAnimation = 2;
         int expectUotable = 1;
         int expectMadhouse = 1;
         int expectA1Pictures = 1;
         foreach (var item in list)
            {
                if (item.Nev == "Kyoto Animation")
                {
                    Assert.That(item.Count == expectKyotoAnimation);
                }
                else if (item.Nev == "Ufotable")
                {
                    Assert.That(item.Count == expectUotable);
                }
                else if (item.Nev == "A1 Pictures")
                {
                    Assert.That(item.Count == expectA1Pictures);
                }
                else if (item.Nev == "Madhouse")
                {
                    Assert.That(item.Count == expectMadhouse);
                }
            }
        }

        /// <summary>
        /// Test Anime per anime studios number.
        /// </summary>
        [Test]
        public void AverageSeasonnumberByAnimeStudios()
        {
            List<AverageSeasonnumberByAnimeStudios> list = this.l.AverageSeasonNumberByAnimeStudios();
            double expectKyotoAnimation = 4;
            double expectUotable = 1;
            double expectMadhouse = 2;
            double expectA1Pictures = 4;
            foreach (var item in list)
            {
                if (item.Nev == "Kyoto Animation")
                {
                    Assert.That(item.Average == expectKyotoAnimation);
                }
                else if (item.Nev == "Ufotable")
                {
                    Assert.That(item.Average == expectUotable);
                }
                else if (item.Nev == "A1 Pictures")
                {
                    Assert.That(item.Average == expectA1Pictures);
                }
                else if (item.Nev == "Madhouse")
                {
                    Assert.That(item.Average == expectMadhouse);
                }
            }
        }
    }
}
