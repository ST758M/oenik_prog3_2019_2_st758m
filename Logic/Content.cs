﻿// <copyright file="Content.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    /// <summary>
    /// Content class.
    /// </summary>
    public class Content
    {
        /// <summary>
        /// Gets or sets iD.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets Studio id.
        /// </summary>
        public int STUDIO_ID { get; set; }

        /// <summary>
        /// Gets or sets nev.
        /// </summary>
        public string NEV { get; set; }

        /// <summary>
        /// Gets or sets romanizalt nev.
        /// </summary>
        public string ROMANZALT_NEV { get; set; }

        /// <summary>
        /// Gets or sets forras anyag.
        /// </summary>
        public string FORRAS_ANYAG { get; set; }

        /// <summary>
        /// Gets or sets evadok szama.
        /// </summary>
        public int EVADOK_SZAMA { get; set; }

        /// <summary>
        /// Gets or sets kozvetites eve.
        /// </summary>
        public int KOZVETITES_EVE { get; set; }
    }
}
