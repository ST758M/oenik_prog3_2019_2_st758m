﻿// <copyright file="RootObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Json.Net;
    using NUnit;
    using Repository;

    /// <summary>
    /// RootObject.
    /// </summary>
    public class RootObject
    {
        /// <summary>
        /// Gets or sets list of objects.
        /// </summary>
        public List<Content> Contents { get; set; }
    }
}
