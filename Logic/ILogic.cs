﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using Data;
    using Json.Net;
    using NUnit;
    using Repository;

    /// <summary>
    /// ILogic interface.
    /// </summary>
    public interface ILogic : ICRUD_Operations
    {
        /// <summary>
        /// anime studio.
        /// </summary>
        /// <param name="anime_studio">object parameter.</param>
        new void Add_anime_studio(ANIME_STUDIOS anime_studio);

        /// <summary>
        /// character add.
        /// </summary>
        /// <param name="character">object.</param>
        new void Add_character(CHARACTERS character);

        /// <summary>
        /// Add anime.
        /// </summary>
        /// <param name="anime">object.</param>
        new void Add_anime(ANIMES anime);

        /// <summary>
        /// Remove anime studio.
        /// </summary>
        /// <param name="anime_studio">object.</param>
        new void Remove_anime_studio(ANIME_STUDIOS anime_studio);

        /// <summary>
        /// Remove character.
        /// </summary>
        /// <param name="character">object.</param>
        new void Remove_character(CHARACTERS character);

        /// <summary>
        /// Remove anime.
        /// </summary>
        /// <param name="anime">object.</param>
        new void Remove_anime(ANIMES anime);

        /// <summary>
        /// Update anime studio.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        new void Update_anime_studio(int id, string modify);

        /// <summary>
        /// Update character.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        new void Update_character(int id, string modify);

        /// <summary>
        /// Update anime.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        new void Update_anime(int id, string modify);

        /// <summary>
        /// Anime to a List.
        /// </summary>
        /// <returns>Anime List.</returns>
        new List<ANIME_STUDIOS> GetAllanime_studios();

        /// <summary>
        /// Anime to a List.
        /// </summary>
        /// <returns>Anime List.</returns>
        new List<ANIMES> GetAllanimes();

        /// <summary>
        /// Extras to a list.
        /// </summary>
        /// <returns>Character List.</returns>
        new List<CHARACTERS> GetAllcharacter();

        /// <summary>
        /// Is contain an anime studio instance.
        /// </summary>
        /// <param name="instance">object.</param>
        /// <returns>true/false.</returns>
        bool IsContain_anime_studio(ANIME_STUDIOS instance);

        /// <summary>
        /// Is contain anime instance.
        /// </summary>
        /// <param name="instance">object.</param>
        /// <returns>true/false.</returns>
        bool IsContain_animes(ANIMES instance);

        /// <summary>
        /// Is contain an character instance.
        /// </summary>
        /// <param name="instance">object.</param>
        /// <returns>true/false.</returns>
        bool IsContain_character(CHARACTERS instance);

        /// <summary>
        /// Car prices with characters prices summed.
        /// </summary>
        /// <returns>result list.</returns>
        List<IsAnimeContainBlackHairedCharacter> IsAnimeContainBlackHairedCharacter();

        /// <summary>
        /// Average price by anime studios.
        /// </summary>
        /// <returns>result list.</returns>
        List<AnimePerAnimeStudios> AnimePerAnimeStudios();

        /// <summary>
        /// How many characters per animes.
        /// </summary>
        /// <returns>result list.</returns>
        List<AverageSeasonnumberByAnimeStudios> AverageSeasonNumberByAnimeStudios();
    }

    /// <summary>
    /// Struct for non crud method.
    /// </summary>
    public struct IsAnimeContainBlackHairedCharacter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IsAnimeContainBlackHairedCharacter"/> struct.
        /// Constructor.
        /// </summary>
        /// <param name="nev">anime_nev.</param>
        /// <param name="van_e">is contain black hair.</param>
        public IsAnimeContainBlackHairedCharacter(string nev, bool van_e)
        {
            this.Nev = nev;
            this.Van_e = van_e;
        }

        /// <summary>
        /// Gets or sets anime nev.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether contains black color haired character or not.
        /// </summary>
        public bool Van_e { get; set; }
    }

    /// <summary>
    /// Non crud method struct.
    /// </summary>
    public struct AverageSeasonnumberByAnimeStudios
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AverageSeasonnumberByAnimeStudios"/> struct.
        /// Constructor.
        /// </summary>
        /// <param name="nev">anime_nev.</param>
        /// <param name="average">average number by studios.</param>
        public AverageSeasonnumberByAnimeStudios(string nev, double average)
        {
            this.Nev = nev;
            this.Average = average;
        }

        /// <summary>
        /// Gets or sets anime nev.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether contains black color haired character or not.
        /// </summary>
        public double Average { get; set; }
    }

    /// <summary>
    /// Non crud method struct.
    /// </summary>
    public struct AnimePerAnimeStudios
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AnimePerAnimeStudios"/> struct.
        /// Constructor.
        /// </summary>
        /// <param name="nev">anime_nev.</param>
        /// <param name="count">count of animes by studios.</param>
        public AnimePerAnimeStudios(string nev, int count)
        {
            this.Nev = nev;
            this.Count = count;
        }

        /// <summary>
        /// Gets or sets anime nev.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether contains black color haired character or not.
        /// </summary>
        public int Count { get; set; }
    }

    /// <summary>
    /// Logic class.
    /// </summary>
    public class Logic : ILogic
    {
        private readonly ICRUD_Operations cRUDOperations;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Logic constructor.
        /// </summary>
        /// <param name="repository">get repository from data class.</param>
        public Logic(ICRUD_Operations repository)
        {
            this.cRUDOperations = repository;
        }

        /// <summary>
        /// Add character.
        /// </summary>
        /// <param name="character">object character.</param>
        public void Add_character(CHARACTERS character)
        {
            if (character == null)
            {
                throw new ArgumentNullException();
            }
            else if (this.GetAllanimes().Where(x => x.ID == character.ID).Count() != 0)
            {
                throw new Exception("Azonos Id-vel nem lehet hozzáadni karaktert.");
            }

            this.cRUDOperations.Add_character(character);
        }

        /// <summary>
        /// Add anime studio.
        /// </summary>
        /// <param name="anime_studio">object anime studio.</param>
        public void Add_anime_studio(ANIME_STUDIOS anime_studio)
        {
            if (anime_studio == null)
            {
                throw new ArgumentNullException();
            }

            this.cRUDOperations.Add_anime_studio(anime_studio);
        }

        /// <summary>
        /// Add anime.
        /// </summary>
        /// <param name="anime">object anime.</param>
        public void Add_anime(ANIMES anime)
        {
            if (anime == null)
            {
                throw new ArgumentNullException();
            }

            this.cRUDOperations.Add_anime(anime);
        }

        /// <summary>
        /// Is Anime Contains black haired character.
        /// </summary>
        /// <returns>result list.</returns>
        public List<IsAnimeContainBlackHairedCharacter> IsAnimeContainBlackHairedCharacter()
        {
            List<IsAnimeContainBlackHairedCharacter> instanceList = new List<IsAnimeContainBlackHairedCharacter>();
            var a = from x in this.cRUDOperations.GetAllanimes()
                    join y in this.cRUDOperations.GetAllcharacter() on x.ID equals y.ANIME_ID
                    group y by x.NEV into g
                    select new
                    {
                        van_e = g.Where(t => t.HAJSZIN == "fekete").Count() != 0,
                        Nev = g.Key,
                    };

            foreach (var item in a)
            {
                IsAnimeContainBlackHairedCharacter instance = new IsAnimeContainBlackHairedCharacter(item.Nev, item.van_e);
                instanceList.Add(instance);
            }

            return instanceList;
        }

        /// <summary>
        /// How many characters connected to animes.
        /// </summary>
        /// <returns>result list.</returns>
        public List<AverageSeasonnumberByAnimeStudios> AverageSeasonNumberByAnimeStudios()
        {
            List<AverageSeasonnumberByAnimeStudios> instanceList = new List<AverageSeasonnumberByAnimeStudios>();
            var a = from x in this.cRUDOperations.GetAllanime_studios()
                    join
                    y in this.cRUDOperations.GetAllanimes() on x.ID equals y.STUDIO_ID
                    group y by x.NEV into g
                    select new
                    {
                        AverageSeaseon = g.Average(t => t.EVADOK_SZAMA),
                        Anime_nev = g.Key,
                    };

            foreach (var item in a)
            {
                AverageSeasonnumberByAnimeStudios instance = new AverageSeasonnumberByAnimeStudios(item.Anime_nev, (double)item.AverageSeaseon);
                instanceList.Add(instance);
            }

            return instanceList;
        }

        /// <summary>
        /// Average price by anime studios.
        /// </summary>
        /// <returns>result list.</returns>
        public List<AnimePerAnimeStudios> AnimePerAnimeStudios()
        {
            List<AnimePerAnimeStudios> instanceList = new List<AnimePerAnimeStudios>();
            var a = from x in this.cRUDOperations.GetAllanime_studios()
                    from y in this.cRUDOperations.GetAllanimes()
                    where x.ID == y.STUDIO_ID
                    group y by x.NEV into g
                    select new
                    {
                        Animek_szama = g.Count(),
                        Anime_nev = g.Key,
                    };
            foreach (var item in a)
            {
                AnimePerAnimeStudios instance = new AnimePerAnimeStudios(item.Anime_nev, item.Animek_szama);
                instanceList.Add(instance);
            }

            return instanceList;
        }

        /// <summary>
        /// Remove character.
        /// </summary>
        /// <param name="character">object character.</param>
        public void Remove_character(CHARACTERS character)
        {
            this.cRUDOperations.Remove_character(character);
        }

        /// <summary>
        /// Remove anime studio.
        /// </summary>
        /// <param name="anime_studio">object.</param>
        public void Remove_anime_studio(ANIME_STUDIOS anime_studio)
        {
            this.cRUDOperations.Remove_anime_studio(anime_studio);
        }

        /// <summary>
        /// Remove anime.
        /// </summary>
        /// <param name="anime">object anime.</param>
        public void Remove_anime(ANIMES anime)
        {
            this.cRUDOperations.Remove_anime(anime);
        }

        /// <summary>
        /// Update character.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        public void Update_character(int id, string modify)
        {
            this.cRUDOperations.Update_character(id, modify);
        }

        /// <summary>
        /// Update anime studios.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        public void Update_anime_studio(int id, string modify)
        {
            this.cRUDOperations.Update_anime_studio(id, modify);
        }

        /// <summary>
        /// Update anime.
        /// </summary>
        /// <param name="id">object id.</param>
        /// <param name="modify">modify parameters.</param>
        public void Update_anime(int id, string modify)
        {
            this.cRUDOperations.Update_anime(id, modify);
        }

        /// <summary>
        /// anime studio count.
        /// </summary>
        /// <returns>return repo result.</returns>
        public int Count_anime_studio()
        {
            return this.GetAllanime_studios().Count;
        }

        /// <summary>
        /// Anime count.
        /// </summary>
        /// <returns>return repo result.</returns>
        public int Count_anime()
        {
            return this.GetAllanimes().Count;
        }

        /// <summary>
        /// Extras character.
        /// </summary>
        /// <returns>return repo result.</returns>
        public int Count_character()
        {
            return this.GetAllcharacter().Count;
        }

        /// <summary>
        /// Is contain an anime studio instance.
        /// </summary>
        /// <param name="instance">object.</param>
        /// <returns>true/false.</returns>
        public bool IsContain_anime_studio(ANIME_STUDIOS instance)
        {
            foreach (ANIME_STUDIOS item in this.GetAllanime_studios())
            {
                if (instance.ALAPITAS_EVE == item.ALAPITAS_EVE &&
                    instance.DOLGOZOK_SZAMA == item.DOLGOZOK_SZAMA &&
                    instance.ID == item.ID &&
                    instance.KOZPONT == item.KOZPONT &&
                    instance.NEV == item.NEV &&
                    instance.ROMANIZALT_NEV == item.ROMANIZALT_NEV &&
                    instance.TULAJDONOS == item.TULAJDONOS)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Is Contain anime instance.
        /// </summary>
        /// <param name="instance">object.</param>
        /// <returns>true/false.</returns>
        public bool IsContain_animes(ANIMES instance)
        {
            foreach (ANIMES item in this.GetAllanimes())
            {
                if (instance.CHARACTERS == item.CHARACTERS &&
                    instance.EVADOK_SZAMA == item.EVADOK_SZAMA &&
                    instance.ID == item.ID &&
                    instance.FORRAS_ANYAG == item.FORRAS_ANYAG &&
                    instance.NEV == item.NEV &&
                    instance.ROMANIZALT_NEV == item.ROMANIZALT_NEV &&
                    instance.KOZVETITES_EVE == item.KOZVETITES_EVE)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Is Contain character instance.
        /// </summary>
        /// <param name="instance">object.</param>
        /// <returns>true/false.</returns>
        public bool IsContain_character(CHARACTERS instance)
        {
            foreach (CHARACTERS item in this.GetAllcharacter())
            {
                if (instance.HAJSZIN == item.HAJSZIN &&
                    instance.ID == item.ID &&
                    instance.LEGFOBB_TULAJDONSAG == item.LEGFOBB_TULAJDONSAG &&
                    instance.NEV == item.NEV &&
                    instance.ROMANIZALT_NEV == item.ROMANIZALT_NEV &&
                    instance.KOZVETITES_EVE == item.KOZVETITES_EVE &&
                    instance.RUHAZAT_JELLEGE == item.RUHAZAT_JELLEGE &&
                    instance.ANIME_ID == item.ANIME_ID)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get all anime studio.
        /// </summary>
        /// <returns>anime studio list.</returns>
        public List<ANIME_STUDIOS> GetAllanime_studios()
        {
            return this.cRUDOperations.GetAllanime_studios();
        }

        /// <summary>
        /// Get all anime.
        /// </summary>
        /// <returns>anime list.</returns>
        public List<ANIMES> GetAllanimes()
        {
            return this.cRUDOperations.GetAllanimes();
        }

        /// <summary>
        /// Get all character.
        /// </summary>
        /// <returns>character list.</returns>
        public List<CHARACTERS> GetAllcharacter()
        {
            return this.cRUDOperations.GetAllcharacter();
        }

        /// <summary>
        /// Studios from json.
        /// </summary>
        /// <returns> A list of anime studios.</returns>
        public RootObject StudiosFromJson()
        {
            string urlAddress = "http://localhost:8084/GetJson/JsonReceiver?db=6";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (string.IsNullOrWhiteSpace(response.CharacterSet))
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
                }

                string data = readStream.ReadToEnd();
                string cleanData = data.Replace("\n", string.Empty);
                cleanData = cleanData.Replace("\t", string.Empty);
                cleanData = cleanData.Replace("\\", string.Empty);
                response.Close();
                readStream.Close();

                RootObject deserializedObject = JsonNet.Deserialize<RootObject>(data);
                Console.WriteLine();
                return deserializedObject;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Character by id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Character instance.</returns>
        public CHARACTERS GetCharacterByID(int id)
        {
            return this.cRUDOperations.GetCharacterByID(id);
        }

        /// <summary>
        /// Anime by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>Anime instance.</returns>
        public ANIME_STUDIOS GetAnimeStudioById(int id)
        {
           return this.cRUDOperations.GetAnimeStudioById(id);
        }

        /// <summary>
        /// Anime by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>anime instance.</returns>
        public ANIMES GetAnimeById(int id)
        {
            return this.cRUDOperations.GetAnimeById(id);
        }
    }
}
