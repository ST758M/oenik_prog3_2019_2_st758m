﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Oenik_prog3_2019_2_st758m
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic;
    using Repository;

    /// <summary>
    /// Main program.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            Logic logic = new Logic(new Repo());

            TopMenu();

            void TopMenu()
            {
                Console.WriteLine("Start");
                Console.WriteLine("New Start");

                Console.WriteLine("Udvozoljuk az anime stúdiók rendszerében");
                Console.WriteLine();
                Console.WriteLine("1.Listázás");
                Console.WriteLine("2. Hozzáadása");
                Console.WriteLine("3. Módosítás");
                Console.WriteLine("4. Törlés");
                Console.WriteLine("5. Anime-k száma stúdiónként");
                Console.WriteLine("6. Átlagos évadok száma stúdiónként");
                Console.WriteLine("7. Az anime-ben szerepel -e fekete hajú játékos");
                Console.WriteLine("8. Anime Studiok webes forrásból");

                int num = int.Parse(Console.ReadLine());

                if (num == 1)
                {
                    Listazas();
                }

                if (num == 2)
                {
                    Hozzaadas();
                }

                if (num == 3)
                {
                    Modositas();
                }

                if (num == 4)
                {
                    Torles();
                }

                if (num == 5)
                {
                    AnimePerAnimeStudios();
                }

                if (num == 6)
                {
                    AverageSeasonnumberByAnimeStudios();
                }

                if (num == 7)
                {
                    IsAnimeContainBlackHairedCharacter();
                }

                if (num == 8)
                {
                    AnimeStudiosFromJavaSource();
                }

                Console.ReadLine();
                TopMenu();
            }

            void Listazas()
            {
                Console.Clear();
                Console.WriteLine("Mit szeretne listázni?");
                Console.WriteLine("1. Anime studiok");
                Console.WriteLine("2. Animék");
                Console.WriteLine("3. Karakterek");
                Console.WriteLine("4. Visszalépés");

                int num = int.Parse(Console.ReadLine());

                if (num == 1)
                {
                    Listing_anime_studios();
                }
                else if (num == 2)
                {
                    Listing_animes();
                }
                else if (num == 3)
                {
                    Listing_characters();
                }
                else
                {
                    TopMenu();
                }

                Console.ReadLine();
                Listazas();
            }

            void Hozzaadas()
            {
                Console.Clear();
                Console.WriteLine("Mit szeretne hozzáadni?");
                Console.WriteLine("1. anime studio");
                Console.WriteLine("2. anime");
                Console.WriteLine("3. karakter");
                Console.WriteLine("4. Visszalépés");

                int num = int.Parse(Console.ReadLine());

                if (num == 1)
                {
                    Listing_anime_studios();
                    Console.WriteLine("Kérem adja meg az anime studio paramétereit vesszővel elválasztva");
                    Console.WriteLine("id,  név,    romaziált név,    alapitás éve, kozpont,	tulajdonos, dolgozok száma");
                    string newinstance = Console.ReadLine();
                    string[] datasplit = newinstance.Split(',');

                    ANIME_STUDIOS newobject = new ANIME_STUDIOS()
                    {
                        ID = int.Parse(datasplit[0]),
                        NEV = datasplit[1],
                        ROMANIZALT_NEV = datasplit[2],
                        ALAPITAS_EVE = int.Parse(datasplit[3]),
                        KOZPONT = datasplit[4],
                        TULAJDONOS = datasplit[5],
                        DOLGOZOK_SZAMA = int.Parse(datasplit[6]),
                    };
                    logic.Add_anime_studio(newobject);
                }
                else if (num == 2)
                {
                    Listing_animes();
                    Console.WriteLine("Kérem adja meg az anime paramétereit vesszővel elválasztva");
                    Console.WriteLine("id	studio id	nev	romanizált név	forrás anyag	évadok száma közvetités éve");

                    string newinstance = Console.ReadLine();
                    string[] datasplit = newinstance.Split(',');

                    ANIMES newobject = new ANIMES()
                    {
                        ID = int.Parse(datasplit[0]),
                        STUDIO_ID = int.Parse(datasplit[1]),
                        NEV = datasplit[2],
                        ROMANIZALT_NEV = datasplit[3],
                        FORRAS_ANYAG = datasplit[4],
                        EVADOK_SZAMA = int.Parse(datasplit[5]),
                        KOZVETITES_EVE = int.Parse(datasplit[6]),
                    };

                    logic.Add_anime(newobject);
                }
                else if (num == 3)
                {
                    Listing_characters();
                    Console.WriteLine("Kérem adja meg a karakter paramétereit vesszővel elválasztva");
                    Console.WriteLine("id	anime id	nev	romanizált név	hajszin	legfobb tulajdonsag közvetités éve ruházat jellege");

                    string newinstance = Console.ReadLine();
                    string[] datasplit = newinstance.Split(',');

                    CHARACTERS newobject = new CHARACTERS()
                    {
                        ID = int.Parse(datasplit[0]),
                        ANIME_ID = int.Parse(datasplit[1]),
                        NEV = datasplit[2],
                        ROMANIZALT_NEV = datasplit[3],
                        HAJSZIN = datasplit[4],
                        LEGFOBB_TULAJDONSAG = datasplit[5],
                        KOZVETITES_EVE = int.Parse(datasplit[6]),
                        RUHAZAT_JELLEGE = datasplit[7],
                    };
                    logic.Add_character(newobject);
                }
                else
                {
                    TopMenu();
                }

                Hozzaadas();
            }

            void Modositas()
            {
                Console.Clear();
                Console.WriteLine("Mit szeretne módosítani?");
                Console.WriteLine("1. anime studio");
                Console.WriteLine("2. anime");
                Console.WriteLine("3. karakter");
                Console.WriteLine("4. Visszalépés");

                int num = int.Parse(Console.ReadLine());

                if (num == 1)
                {
                    Listing_anime_studios();
                    Console.WriteLine("Add meg az id-jét annak a sornak amelyet módosítani szeretnél");
                    int id = int.Parse(Console.ReadLine());
                    Console.WriteLine("Add meg azon tulajdonságok értékeit amelyet meg akarsz változtatni, sorrendben," +
                        " vesszővel elválasztva, ha egy tulajdonságot nem szeretnél megváltoztatni írj oda null értéket" +
                        "id,  név,    központ,  romanizált név,  alapitás éve,	tulajdonos, dolgozok száma");
                    string modify = Console.ReadLine();
                    logic.Update_anime_studio(id, modify);
                }
                else if (num == 2)
                {
                    Listing_animes();
                    Console.WriteLine("Add meg a modell-id-jét annak a sornak amelyet módosítani szeretnél");
                    int id = int.Parse(Console.ReadLine());
                    Console.WriteLine("Add meg azon tulajdonságok értékeit amelyet meg akarsz változtatni, sorrendben," +
                        " vesszővel elválasztva, ha egy tulajdonságot nem szeretnél megváltoztatni írj oda null értéket" +
                        "id,	studio id,	nev,	romanizált név,	forrás anyag,	évadok száma, közvetités éve");
                    string modify = Console.ReadLine();
                    logic.Update_anime_studio(id, modify);
                }
                else if (num == 3)
                {
                    Listing_characters();
                    Console.WriteLine("Add meg a modell-id-jét annak a sornak amelyet módosítani szeretnél");
                    int id = int.Parse(Console.ReadLine());
                    Console.WriteLine("Add meg azon tulajdonságok értékeit amelyet meg akarsz változtatni, sorrendben," +
                        " vesszővel elválasztva, ha egy tulajdonságot nem szeretnél megváltoztatni írj oda null értéket" +
                        "id,	anime id,	nev,	romanizált név,	hajszin,	legfobb tulajdonsag, közvetités éve, ruházat jellege");
                    string modify = Console.ReadLine();
                    logic.Update_character(id, modify);
                }
            }

            void Torles()
            {
                Console.Clear();
                Console.WriteLine("Mit szeretne törölni?");
                Console.WriteLine("1. anime studio");
                Console.WriteLine("2. anime");
                Console.WriteLine("3. karakter");
                Console.WriteLine("4. Visszalépés");

                int num = int.Parse(Console.ReadLine());

                if (num == 1)
                {
                    Listing_anime_studios();
                    Console.WriteLine("Kérem adja meg az anime studio paramétereit vesszővel elválasztva");
                    Console.WriteLine("id,  név,    romaziált név,    alapitás éve, kozpont,	tulajdonos, dolgozok száma");
                    string newinstance = Console.ReadLine();
                    string[] datasplit = newinstance.Split(',');

                    ANIME_STUDIOS newobject = new ANIME_STUDIOS()
                    {
                        ID = int.Parse(datasplit[0]),
                        NEV = datasplit[1],
                        ROMANIZALT_NEV = datasplit[2],
                        ALAPITAS_EVE = int.Parse(datasplit[3]),
                        KOZPONT = datasplit[4],
                        TULAJDONOS = datasplit[5],
                        DOLGOZOK_SZAMA = int.Parse(datasplit[6]),
                    };
                    logic.Remove_anime_studio(newobject);
                }
                else if (num == 2)
                {
                    Listing_animes();
                    Console.WriteLine("Kérem adja meg az anime paramétereit vesszővel elválasztva");
                    Console.WriteLine("id	studio id	nev	romanizált név	forrás anyag	évadok száma közvetités éve");

                    string newinstance = Console.ReadLine();
                    string[] datasplit = newinstance.Split(',');

                    ANIMES newobject = new ANIMES()
                    {
                        ID = int.Parse(datasplit[0]),
                        STUDIO_ID = int.Parse(datasplit[1]),
                        NEV = datasplit[2],
                        ROMANIZALT_NEV = datasplit[3],
                        FORRAS_ANYAG = datasplit[4],
                        EVADOK_SZAMA = int.Parse(datasplit[5]),
                        KOZVETITES_EVE = int.Parse(datasplit[6]),
                    };

                    logic.Remove_anime(newobject);
                }
                else if (num == 3)
                {
                    Listing_characters();
                    Console.WriteLine("Kérem adja meg a karakter paramétereit vesszővel elválasztva");
                    Console.WriteLine("id	anime id	nev	romanizált név	hajszin	legfobb tulajdonsag közvetités éve ruházat jellege");

                    string newinstance = Console.ReadLine();
                    string[] datasplit = newinstance.Split(',');

                    CHARACTERS newobject = new CHARACTERS()
                    {
                        ID = int.Parse(datasplit[0]),
                        ANIME_ID = int.Parse(datasplit[1]),
                        NEV = datasplit[2],
                        ROMANIZALT_NEV = datasplit[3],
                        HAJSZIN = datasplit[4],
                        LEGFOBB_TULAJDONSAG = datasplit[5],
                        KOZVETITES_EVE = int.Parse(datasplit[6]),
                        RUHAZAT_JELLEGE = datasplit[7],
                    };
                    logic.Remove_character(newobject);
                }
                else
                {
                    TopMenu();
                }

                Torles();
            }

            void AnimePerAnimeStudios()
            {
                List<AnimePerAnimeStudios> result = new List<AnimePerAnimeStudios>();
                result = logic.AnimePerAnimeStudios();
                foreach (var item in result)
                {
                    Console.WriteLine(item.Nev + "    " + item.Count);
                }
            }

            void AverageSeasonnumberByAnimeStudios()
            {
                List<AverageSeasonnumberByAnimeStudios> result = new List<AverageSeasonnumberByAnimeStudios>();
                result = logic.AverageSeasonNumberByAnimeStudios();

                foreach (var item in result)
                {
                    Console.WriteLine(item.Nev + "   " + item.Average);
                }
            }

            void Listing_anime_studios()
            {
                List<ANIME_STUDIOS> result = new List<ANIME_STUDIOS>();
                result = logic.GetAllanime_studios();

                foreach (var item in result)
                {
                    Console.WriteLine(item.ID + "," + item.ALAPITAS_EVE + "," + item.DOLGOZOK_SZAMA + "," + item.KOZPONT + "," + item.NEV + "," +
                        item.ROMANIZALT_NEV + "," + item.TULAJDONOS);
                }
            }

            void Listing_animes()
            {
                List<ANIMES> result = new List<ANIMES>();
                result = logic.GetAllanimes();

                foreach (ANIMES item in result)
                {
                    Console.WriteLine(item.ID + "," + item.FORRAS_ANYAG + "," + item.EVADOK_SZAMA + "," + item.KOZVETITES_EVE + "," +
                        item.NEV + "," + item.ROMANIZALT_NEV + "," + item.STUDIO_ID);
                }
            }

            void Listing_characters()
            {
                List<CHARACTERS> result = new List<CHARACTERS>();
                result = logic.GetAllcharacter();

                foreach (CHARACTERS item in result)
                {
                    Console.WriteLine(item.ID + "," + item.HAJSZIN + "," + item.KOZVETITES_EVE + "," + item.LEGFOBB_TULAJDONSAG + "," +
                        item.NEV + "," +
                        item.ROMANIZALT_NEV + "," + item.RUHAZAT_JELLEGE);
                }
            }

            void IsAnimeContainBlackHairedCharacter()
            {
                List<IsAnimeContainBlackHairedCharacter> result = new List<IsAnimeContainBlackHairedCharacter>();
                result = logic.IsAnimeContainBlackHairedCharacter();

                foreach (var item in result)
                {
                    Console.WriteLine(item.Nev + "    " + item.Van_e);
                }
            }

            void AnimeStudiosFromJavaSource()
            {
                RootObject sourceList = logic.StudiosFromJson();

                if (sourceList == null)
                {
                    Console.WriteLine("Hibás response!");
                }
                else
                {
                    foreach (var item in sourceList.Contents)
                    {
                        Console.WriteLine(item.ID + " " + item.STUDIO_ID + " " + item.NEV + " " + item.ROMANZALT_NEV
                            + " " + item.FORRAS_ANYAG + " " + item.EVADOK_SZAMA + " " + item.KOZVETITES_EVE);
                    }
                }
            }
        }
    }
}
